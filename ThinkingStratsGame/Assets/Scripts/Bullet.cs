﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	public float LifeSpan;
	public Transform tf;
	public Vector3 direction;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		Destroy (gameObject, LifeSpan);

		direction = Vector3.right;
	}
	
	// Update is called once per frame
	void Update () {
		tf.Translate (direction * Time.deltaTime * speed);
	}
	void OnTriggerEnter2D(Collider2D other){
		//killplayer
		Debug.Log(other.tag);

		if (other.gameObject.tag != "Player") {
			Debug.Log("Got there");
			
			Destroy (gameObject);
		}

	}

}
