﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
	public GameObject Bullet;
	public Transform tf;
	public Animator an;
	public float fireRate;
	public float timerCurrent;
	void Start(){
		tf = GetComponent<Transform> ();
		an = GetComponent<Animator> ();
	}
	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.isDed) {
			timerCurrent += Time.deltaTime;
			if (timerCurrent >= fireRate) {
				timerCurrent = 0;
				if (Input.GetKey (KeyCode.Space)) {
					an.Play ("GunA");
					Instantiate (Bullet, tf.position, tf.rotation);
				}
			}
		}
	}
}
