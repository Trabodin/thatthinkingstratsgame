﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
    public Transform tf;
	Vector3 mousePosition;
	Vector3 objectPos;


	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.isDed) {
			Vector2 direction = Camera.main.ScreenToWorldPoint (Input.mousePosition) - tf.position;
			float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
			Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
			tf.rotation = rotation;

			if (Input.GetKey (KeyCode.W)) {
				tf.position += Vector3.up * Time.deltaTime * GameManager.instance.MoveSpeed;
			}
			if (Input.GetKey (KeyCode.S)) {
				tf.position += Vector3.down * Time.deltaTime * GameManager.instance.MoveSpeed;
			}
			if (Input.GetKey (KeyCode.D)) {
				tf.position += Vector3.right * Time.deltaTime * GameManager.instance.MoveSpeed;
			}
			if (Input.GetKey (KeyCode.A)) {
				tf.position += Vector3.left * Time.deltaTime * GameManager.instance.MoveSpeed;
			}
		}
    }
}
