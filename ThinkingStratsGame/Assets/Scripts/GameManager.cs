﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public GameObject Player;
    public float MoveSpeed;

	public GameObject Spider;
	public GameObject BossSpider;
	public float Spawnsize;
	public List<GameObject> Enemies;
	public int MaxEnemies;

	public float MaxTime;
	public float currentTime;

	public bool isDed = false;
	bool bossSpawned = false;
	public GameObject DeathScreen;


	// Use this for initialization
	void Start () {

		currentTime = MaxTime;

        if ( instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDed) {
			currentTime -= Time.deltaTime;

		} 
		else 
		{
			DeathScreen.SetActive (true);
		}
		Vector3 newPos = Random.insideUnitCircle * Spawnsize;
		if (Vector3.Distance (newPos, Player.transform.position) > Spawnsize/4f) {
			if (Enemies.Count < MaxEnemies + (MaxTime - currentTime)) {
				Instantiate (Spider, newPos, Quaternion.identity);
			}
			if ((MaxTime - currentTime) > 50 && !bossSpawned) {
				bossSpawned = true;
				Instantiate (BossSpider, newPos, Quaternion.identity);
			}
		}
	}
}
