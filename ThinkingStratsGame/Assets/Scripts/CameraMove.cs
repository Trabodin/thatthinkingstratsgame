﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
    public Transform tf;
	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.isDed) {
			tf.position = GameManager.instance.Player.transform.position + new Vector3 (0, 0, -10);
		}
	}
}
