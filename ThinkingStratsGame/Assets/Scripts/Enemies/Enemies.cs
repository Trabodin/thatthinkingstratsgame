﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemies : MonoBehaviour {
	public Transform tf;
	public float Health;
	public float Speed;

	public void Start(){
		tf = GetComponent<Transform> ();
		GameManager.instance.Enemies.Add (gameObject);
	}

}
