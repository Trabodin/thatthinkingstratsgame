﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpider : Enemies {
	// Update is called once per frame
	void Update () {
		Vector3 dir = GameManager.instance.Player.transform.position - tf.position;
		dir.Normalize ();

		tf.Translate (dir * Time.deltaTime * Speed);
	}
	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Player") {
			GameManager.instance.isDed = true;
		}
	}

	/*void OnTriggerEnter2D(Collider2D other){
		GameManager.instance.Enemies.Remove (gameObject);
		Destroy (gameObject);
	}*/
}
