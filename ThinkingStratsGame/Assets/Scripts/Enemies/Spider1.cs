﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Spider1 : Enemies {

	public AudioClip Death;

	// Update is called once per frame
	void Update () {
		Vector3 dir = GameManager.instance.Player.transform.position - tf.position;
		dir.Normalize ();

		tf.Translate (dir * Time.deltaTime * Speed);
	}
	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Player") {
			GameManager.instance.isDed = true;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		Debug.Log ("got here");
		GameManager.instance.Enemies.Remove (gameObject);
		AudioSource.PlayClipAtPoint (Death, gameObject.transform.position);
		Destroy (gameObject);
	}
}
